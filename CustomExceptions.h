#pragma once
#include <utility>

enum ErrorCode {
  NO_ERROR,
  UNHANDLED_ERROR,
  IO_ERROR,
  INVALID_INPUT,
  INVALID_PARSE_FILE,
  NOT_IMPLEMENTED_ERROR
};

class OwnException : public std::exception {
 public:
  [[nodiscard]] virtual int getCode() const = 0;
};

class IOException : public OwnException {

 public:
  explicit IOException(std::string msg) : message(std::move(msg)), code(ErrorCode::IO_ERROR) {}
  [[nodiscard]] const char *what() const noexcept override {
	return message.c_str();
  }

  [[nodiscard]] int getCode() const override {
	return code;
  }

 private:
  std::string message;
  ErrorCode code;

};

class InvalidFile : public OwnException {

 public:
  explicit InvalidFile(std::string msg) : message(std::move(msg)), code(ErrorCode::INVALID_PARSE_FILE) {}
  [[nodiscard]] const char *what() const noexcept override {
	return message.c_str();
  }

  [[nodiscard]] int getCode() const override {
	return code;
  }

 private:
  std::string message;
  ErrorCode code;

};

class InvalidInput : public OwnException {

 public:
  explicit InvalidInput(std::string msg) : message(std::move(msg)), code(ErrorCode::INVALID_INPUT) {}
  [[nodiscard]] const char *what() const noexcept override {
	return message.c_str();
  }

  [[nodiscard]] int getCode() const override {
	return code;
  }

 private:
  std::string message;
  ErrorCode code;

};

class NotImplemented : public OwnException {

 public:
  explicit NotImplemented(std::string msg) : message(std::move(msg)), code(ErrorCode::NOT_IMPLEMENTED_ERROR) {}
  [[nodiscard]] const char *what() const noexcept override {
	return message.c_str();
  }

  [[nodiscard]] int getCode() const override {
	return code;
  }

 private:
  std::string message;
  ErrorCode code;

};
