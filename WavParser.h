#pragma once
#include <fstream>
#include <filesystem>
#include "WavFormat.h"

struct ParsedWavInfo {
  WavRiffHeader riffHeader;
  ChunkInfo fmtChunkInfo;
  WavFmtHeader fmtHeader;
  uint32_t dataSize; // NumSamples * NumChannels * BitsPerSample/8
  std::streampos dataPosition;
  std::string fileName;
  size_t samplesCount;
  uint32_t bytesPerSample;
};

[[nodiscard]] ParsedWavInfo parseWavFile(const std::filesystem::path &inputFileName);

